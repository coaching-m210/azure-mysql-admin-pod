# Azure MySQL Admin Pod erstellen

MySQL Client Pod erstellen und einloggen

```
kubectl create namespace mysql
kubectl apply -f mysql-client-pod.yml -n mysql
kubectl exec -it mysql-client -n mysql -- bash
```
